module Pivot
  class Tracker
    # Code here!
    def self.count(items)
      items.size
    end

    def self.item_for(items, assignee)
      assignee = find_assignee(items, assignee)
      if assignee.any?
        assignee.select {|x| x[:name] == 'EREC-14' }.first
      end
    end

    def self.pivoted?(items, assignee)
      assignee = find_assignee(items, assignee)
      (assignee.any?) ? true : false
    end

    def self.total_points(items, assignee = nil)
      if !assignee.nil?
        found_assignee = find_assignee(items, assignee[:assignee])
        if found_assignee.any?
          points = get_points(found_assignee)
          total_points = points.inject(:+)
          return total_points
        end
      else
        points = get_points(items)
        total_points = points.inject(:+)
        if total_points >= 13
          return 13
        end
      end
    end

    def self.unique_assignees(items)
      unique_items = items.uniq { |x| x[:assignee] }
      unique_items if unique_items.any?
    end

    private

    def self.find_assignee(items, assignee)
      items.select { |x| x[:assignee] == assignee }
    end

    def self.get_points(items)
      items.map { |x| x[:points] }
    end

  end
end