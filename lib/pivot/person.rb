module Pivot
  class Person
    # Code here!
    include ActiveModel::Validations

    attr_accessor :email, :first_name, :last_name, :items

    def initialize(person)
      @person = OpenStruct.new(person)
      @email = @person.email
      @first_name = @person.first_name
      @last_name = @person.last_name
      @items = []
    end

    def add_item(item)
      if block_given?
        yield
      end
      item.assignee = @email
      @items << item
    end
  end
end