require 'active_model'

module Pivot
  class Item
    # Code here!
    include ActiveModel::Validations

    attr_accessor :name, :assignee, :points, :project_code

    validate :validate_code

    def initialize(item)
      @allowed_codes = ['EREC', 'AZR']
      @item = OpenStruct.new(item)
      @name = @item.name
      @assignee = @item.assignee
      @points = @item.points
      @project_code = extract_code
    end

    private

    def extract_code
      @item.name.split('-').first
    end

    def validate_code
      unless @allowed_codes.include?(@project_code)
        errors.add(:project_code, 'project code not valid')
      end
    end

  end

end